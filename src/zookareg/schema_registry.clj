(ns zookareg.schema-registry
  "Utilities for configuring, starting and halting an embedded Confluent Schema
  Registry."
  (:require [integrant.core :as ig]
            [me.raynes.fs :as fs]
            [zookareg.utils :as ut])
  (:import [io.confluent.kafka.schemaregistry.rest SchemaRegistryConfig SchemaRegistryRestApplication]
           java.io.File
           org.eclipse.jetty.server.Server))

(defn- ^File ->config-file [config]
  (let [props (ut/m->properties config)
        file  (fs/temp-file "schema-registry" ".properties")]
    (ut/store-properties props file)
    file))

(defn ->schema-registry
  "Starts a Confluent Schema Registry server."
  [config]
  (let [config (SchemaRegistryConfig. (.getAbsolutePath (->config-file config)))
        app    (SchemaRegistryRestApplication. ^SchemaRegistryConfig config)
        ^Server server (.createServer app)]
    (.start server)
    server))

(defn halt!
  "Stops a Confluent Schema Registry server."
  [^Server server]
  (when server
    (.stop server)))

(defmethod ig/init-key ::server [_ {:keys [config]}]
  (->schema-registry config))

(defmethod ig/halt-key! ::server [_ server]
  (halt! server))
