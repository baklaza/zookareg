(ns zookareg.zookeeper
  "Utilities for starting and halting an embedded Curator (Zookeeper)
  TestingServer."
  (:require [integrant.core :as ig])
  (:import org.apache.curator.test.TestingServer))

(defn  ->zk
  "Starts a Curator (Zookeeper) TestingServer on port."
  [^long port]
  (TestingServer. port))

(defn halt!
  "Stops a running TestingServer."
  [^TestingServer zk]
  (when zk
    (.close zk)))

(defmethod ig/init-key ::server [_ {:keys [port]}]
  (->zk port))

(defmethod ig/halt-key! ::server [_ zk]
  (halt! zk))

